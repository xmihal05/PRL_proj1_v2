/*	PRL projekt 1
	Merge-Splitting Sort
	Klara Mihalikova
	xmihal05
*/

 #include <mpi.h>
 #include <iostream>
 #include <fstream>
 #include <vector>
 #include <algorithm>	//pouzite iba pre zoradenie cisel vramci 1 procesora
 
 using namespace std;

 #define TAG 0

 bool compareFunct(int i, int j){
 	//vyuziva sa pre zoranedie cisel vramci 1 procesora
 	return (i<j);
 }

 int main(int argc, char *argv[])
 {
    int numprocs;               //pocet procesoru
    int myid;                   //muj rank
    vector<int> neighnumbers;            //hodnota souseda
    vector<int> allnums;		//vektor vsetkych cisel
    int mysize;					//velkost mojho vektora (mynumbers)
    vector<int> mynumbers;      //moje hodnoty
    int numcount;				//pocet radenych cisel
    MPI_Status stat;            //struct- obsahuje kod- source, tag, error

    //MPI INIT
    MPI_Init(&argc,&argv);                          // inicializace MPI 
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);       // zjistíme, kolik procesů běží 
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);           // zjistíme id svého procesu 
 
    //NACTENI SOUBORU
    /* -proc s rankem 0 nacita vsechny hodnoty
     * -postupne rozesle jednotlive hodnoty vsem i sobe
    */
    //zistim velkost predavaneho vektoru
    if(myid == 0){
    	numcount = 0;	//init pocitadla
        char input[]= "numbers";                          //jmeno souboru    
        int number;                                     //hodnota pri nacitani souboru
        int invar= 0;                                   //invariant- urcuje cislo proc, kteremu se bude posilat
        fstream fin;                                    //cteni ze souboru
        fin.open(input, ios::in);                   
        //ZISTIT KOLKO CISEL ZISKA KAZDY PROC.
        while(fin.good()){  //!!!VLOZIT VNORENY CYKLUS PRE POCET PRVKOV PRE DANY PROCESOR
            number= fin.get();
            if(!fin.good()) 
            	break;                      //nacte i eof, takze vyskocim
            cout<<number<<" ";             //vypisem cislo na cout
            allnums.push_back(number);		//ulozim cislo do vektoru       
            numcount++;						//inkrementujem pocitadlo     
        }//while
        cout << '\n';	//odriadkujem po vypise cisel        
        fin.close();      

        //vypocitam velkost predavanych dat
        int numsPerProc = numcount / numprocs;

        //poposielam velkost predavaneho vektoru procesorom
        int i = numcount % numprocs;
        while(invar < numprocs){
        	if(i > 0){	//modulo nebolo rovne nula, dany procesor radi viac cisel
        		int newNum = numsPerProc+1;
        		MPI_Send(&newNum, 1, MPI_INT, invar, TAG, MPI_COMM_WORLD); //buffer,velikost,typ,rank prijemce,tag,komunikacni skupina
        		i--;	//po priradeni znizim pocet vacsich velkosti
        	}
        	//inak predam vypocitanu velkost
        	else MPI_Send(&numsPerProc, 1, MPI_INT, invar, TAG, MPI_COMM_WORLD); //buffer,velikost,typ,rank prijemce,tag,komunikacni skupina
            invar++;	//inkrementujem id procesoru
        }
        

    }//nacteni souboru

    //PRIJETI HODNOTY CISLA
    //vsechny procesory(vcetne mastera) prijmou hodnotu a zahlasi ji
    //najskor prijmem velkost predavaneho vektoru
    MPI_Recv(&mysize, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); //buffer,velikost,typ,rank odesilatele,tag, skupina, stat
    //cout<<"i am:"<<myid<<" my number is:"<<mynumber<<endl;

    if(myid == 0){
    	int procid = 0;	//id procesora
    	int maxnum = 0;	//maximalny pocet predavanych cisel
    	int vecNumId = 0;	//id predavaneho cisla vo vektore

    	//vypocitam velkost predavanych dat
        int numsPerProc = numcount / numprocs;
        int moduloNumber = numcount % numprocs;

    	while(procid < numprocs){
    		vector<int> v;	//vektor pre dany proc
    		if(moduloNumber > 0){
    			maxnum = numsPerProc+1;	//zvysim pocet predavanych prvkov
    			moduloNumber--;	//dekrementujem pocet vacsich prvkov
    		}
    		else maxnum = numsPerProc;

    		//naplnim vektor
    		for(int i = 0; i < maxnum; i++){
    			v.push_back(allnums.at(vecNumId));
    			vecNumId++;	//zvysim id vo vektore
    		}

    		//odoslem data procesoru
    		MPI_Send(&v[0], maxnum, MPI_INT, procid, TAG, MPI_COMM_WORLD);
    		procid++;	//inkrementujem cislo procesoru
    	}
    }

	//prijmem predany vektor cisel
	mynumbers.resize(mysize);
    MPI_Recv(&mynumbers[0], mysize, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat); //buffer,velikost,typ,rank odesilatele,tag, skupina, stat

    //LIMIT PRO INDEXY
    int oddlimit= 2*(numprocs/2)-1;                 //limity pro sude
    int evenlimit= 2*((numprocs-1)/2);              //liche
    int halfcycles= numprocs/2;
    if((numprocs%2) != 0)
    	halfcycles++;
    int cycles=0;                                   //pocet cyklu pro pocitani slozitosti
    //if(myid == 0) cout<<oddlimit<<":"<<evenlimit<<endl;

    
    //RAZENI------------chtelo by to umet pocitat cykly nebo neco na testy------
    //predpriprava --> zoradenie vlastnej postupnosti
    sort(mynumbers.begin(), mynumbers.end(), compareFunct);

    //cyklus pro linearitu
    for(int j=1; j<=halfcycles; j++){
        cycles++;           //pocitame cykly, abysme mohli udelat krasnej graf:)

        //sude proc 
        if((!(myid%2) || myid==0) && (myid<oddlimit)){
        	//najskor poslem susedovi svoju velkost
            int mynewsize = (int)mynumbers.size();
            MPI_Send(&mynewsize, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD);          //poslu sousedovi svoje cislo
            //potom zasielam svoj vektor
            MPI_Send(&mynumbers[0], (int)mynumbers.size(), MPI_INT, myid+1, TAG, MPI_COMM_WORLD);

            //cakam na velkost mojho vrateneho vektora
            mynewsize = 0;
            MPI_Recv(&mynewsize, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);
            mynumbers.resize(mynewsize);
            //prijmem svoj novy vektor
            MPI_Recv(&mynumbers[0], mynewsize, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);   //a cekam na nizsi
            //cout<<"ss: "<<myid<<endl;
        }//if sude
        else if(myid<=oddlimit){//liche prijimaji zpravu a vraceji mensi hodnotu (to je ten swap)

        	//prijmem velkost predavaneho vektoru
        	int neighsize;
            MPI_Recv(&neighsize, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat); //jsem lichy a prijimam

            neighnumbers.resize(neighsize);
            //prijmem susedov vektor
            MPI_Recv(&neighnumbers[0], neighsize, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat);

            //vlozim cisla zo susedovho vektoru ku mne
            for(int i = 0; i < neighsize; i++)
            	mynumbers.push_back(neighnumbers.at(i));

            //zoradim cisla vo svojom vektore
            sort(mynumbers.begin(), mynumbers.end(), compareFunct);

    		//vycistim susedov vektor
    		neighnumbers.clear();

    		//rozdelim opat do 2 vektorov
    		//zistim "polovicu"
    		int middle = (int)mynumbers.size() / 2;
    		if(((int)mynumbers.size() % 2) != 0)
    			middle++;	//lava strana dostane o cislo viac

    		//naplnim novy susedov vektor
    		for (int i = 0; i < middle; i++)
    			neighnumbers.push_back(mynumbers.at(i));
    		//svoj vektor
    		vector<int> myNewVec;
    		for (int i = middle; i < (int)mynumbers.size(); i++)
    			myNewVec.push_back(mynumbers.at(i));

    		//zaslem mu ho
    		//zaslem mu jeho velkost 
    		neighsize = (int)neighnumbers.size();                                          
            MPI_Send(&neighsize, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD); 
            //zaslem mu vektor
            MPI_Send(&neighnumbers[0], neighsize, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);    

            
            //nastavim svoj novy vektor
            mynumbers.clear();
            for (int i = 0; i < (int)myNewVec.size(); i++)
            	mynumbers.push_back(myNewVec.at(i));
        }//else if (liche)
        else{//sem muze vlezt jen proc, co je na konci
        }//else

        //liche proc 
        if((myid%2) && (myid<evenlimit)){
            //najskor poslem susedovi svoju velkost
            int mynewsize = (int)mynumbers.size();
            MPI_Send(&mynewsize, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD);          //poslu sousedovi svoje cislo
            //potom zasielam svoj vektor
            MPI_Send(&mynumbers[0], (int)mynumbers.size(), MPI_INT, myid+1, TAG, MPI_COMM_WORLD);

            //cakam na velkost mojho vrateneho vektora
            mynewsize = 0;
            MPI_Recv(&mynewsize, 1, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);
            mynumbers.resize(mynewsize);
            //prijmem svoj novy vektor
            MPI_Recv(&mynumbers[0], mynewsize, MPI_INT, myid+1, TAG, MPI_COMM_WORLD, &stat);   //a cekam na nizsi
            //cout<<"ll: "<<myid<<endl;
        }//if liche
        else if(myid<=evenlimit && myid!=0){//sude prijimaji zpravu a vraceji mensi hodnotu (to je ten swap)
            //prijmem velkost predavaneho vektoru
        	int neighsize;
            MPI_Recv(&neighsize, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat); //jsem lichy a prijimam

            neighnumbers.resize(neighsize);
            //prijmem susedov vektor
            MPI_Recv(&neighnumbers[0], neighsize, MPI_INT, myid-1, TAG, MPI_COMM_WORLD, &stat);

            //vlozim cisla zo susedovho vektoru ku mne
            for(int i = 0; i < neighsize; i++)
            	mynumbers.push_back(neighnumbers.at(i));

            //zoradim cisla vo svojom vektore
            sort(mynumbers.begin(), mynumbers.end(), compareFunct);

    		//vycistim susedov vektor
    		neighnumbers.clear();

    		//rozdelim opat do 2 vektorov
    		//zistim "polovicu"
    		int middle = (int)mynumbers.size() / 2;
    		if(((int)mynumbers.size() % 2) != 0)
    			middle++;	//lava strana dostane o cislo viac

    		//naplnim novy susedov vektor
    		for (int i = 0; i < middle; i++)
    			neighnumbers.push_back(mynumbers.at(i));
    		//svoj vektor
    		vector<int> myNewVec;
    		for (int i = middle; i < (int)mynumbers.size(); i++)
    			myNewVec.push_back(mynumbers.at(i));

    		//zaslem mu ho
    		//zaslem mu jeho velkost 
    		neighsize = (int)neighnumbers.size();                                          
            MPI_Send(&neighsize, 1, MPI_INT, myid-1, TAG, MPI_COMM_WORLD); 
            //zaslem mu vektor
            MPI_Send(&neighnumbers[0], neighsize, MPI_INT, myid-1, TAG, MPI_COMM_WORLD);    

            
            //nastavim svoj novy vektor
            mynumbers.clear();
            for (int i = 0; i < (int)myNewVec.size(); i++)
            	mynumbers.push_back(myNewVec.at(i));
        }//else if (sude)
        else{//sem muze vlezt jen proc, co je na konci
        }//else
       
    }//for pro linearitu
    //RAZENI--------------------------------------------------------------------
	
    //FINALNI DISTRIBUCE VYSLEDKU K MASTEROVI-----------------------------------
    int* final= new int [numprocs];
    //final=(int*) malloc(numprocs*sizeof(int));
    for(int i=1; i<numprocs; i++){
       if(myid == i){	//nie som master posielam svoje data
       		//poslem velkost dat
       		int size = (int)mynumbers.size();
			MPI_Send(&size, 1, MPI_INT, 0, TAG,  MPI_COMM_WORLD);

			//poslem data
			MPI_Send(&mynumbers[0], size, MPI_INT, 0, TAG,  MPI_COMM_WORLD);
       } 
       if(myid == 0){	//som master prijmam data
       		int neighsize;
           	MPI_Recv(&neighsize, 1, MPI_INT, i, TAG, MPI_COMM_WORLD, &stat); //jsem 0 a prijimam
           	neighnumbers.resize(neighsize);

           	MPI_Recv(&neighnumbers[0], neighsize, MPI_INT, i, TAG, MPI_COMM_WORLD, &stat); //jsem 0 a prijimam
           
           	//naplnim cisla do svojho vektora
           	for(int j = 0; j < neighsize; j++)
           		mynumbers.push_back(neighnumbers.at(j));

       }//if sem master
    }//for

    //vypis cisel po riadkoch!
    if(myid == 0){
        //cout<<cycles<<endl;
        for (int i = 0; i < numcount; i++)
        	cout << mynumbers.at(i) << '\n';
    }//if vypis
    //cout<<"i am:"<<myid<<" my number is:"<<mynumber<<endl;
    //VYSLEDKY------------------------------------------------------------------
	
 
    MPI_Finalize(); 
    return 0;

 }//main

