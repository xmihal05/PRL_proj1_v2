#!/bin/bash

#pocet cisel bud zadam nebo 15
if [ $# -lt 1 ];then 
    poc_hodnot=10;
else
    poc_hodnot=$1;
fi;
#pocet procesorov je bud zadany alebo 4
if [ $# -lt 2 ]; then
	poc_procesorov=4;
else
	poc_procesorov=$2;
fi;

#preklad cpp zdrojaku
mpic++ -std=c++11 -o mss mss.cpp # --prefix /usr/local/share/OpenMPI 


#vyrobeni souboru s random cisly
dd if=/dev/random bs=1 count=$poc_hodnot of=numbers

#spusteni
mpirun -np $poc_procesorov mss # --prefix /usr/local/share/OpenMPI

#uklid
rm -f mss numbers